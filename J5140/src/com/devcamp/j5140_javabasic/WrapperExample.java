package com.devcamp.j5140_javabasic;

public class WrapperExample {
    public static void autoBoxing() {
        byte bte = 15;
        short sh = 25;
        int it = 35;
        long lng = 45;
        float fat = 55.0F;
        double dbl = 65.0D;
        char ch = 'f';
        boolean bool = false;
        /***
        * //Autoboxing: Converting primitives into objects
        * Autoboxing là cơ chế tự động chuyển đổi kiểu dữ liệu nguyên thủy sang object
        * của wrapper class tương ứng.
        */
        Byte byteobj = bte;
        Short shortobj = sh;
        Integer intobj = it;
        Long longobj = lng;
        Float floatobj=fat;
        Double doubleobj = dbl;
        Character charobj = ch;
        Boolean boolobj = bool;

        System.out.println("---Printing object values (In gia tri cua object)---");
        System.out.println("Byte object: " + byteobj);
        System.out.println("Short object: " + shortobj);
        System.out.println("Integer object: " + intobj);
        System.out.println("Long object: " + longobj);
        System.out.println("Float object: " + floatobj);
        System.out.println("Double object: " + doubleobj);
        System.out.println("Character object: " + charobj);
        System.out.println("Boolean object: " + boolobj);
    }

    public static void unBoxing(){
        byte bte = 15;
        short sh = 25;
        int it = 35;
        long lng = 45;
        float fat = 55.0F;
        double dbl = 65.0D;
        char ch = 'f';
        boolean bool = false;
        //Autoboxing: Converting primitives into objects
        Byte byteobj = bte;
        Short shortobj = sh;
        Integer intobj = it;
        Long longobj = lng;
        Float floatobj = fat;
        Double doubleobj = dbl;
        Character charobj = ch;
        Boolean boolobj = bool;
        /***
        * Unboxing: Converting Objects to Primitives
        * Unboxing là cơ chế tự động chuyển đổi các object
        * của wrapper class sang kiểu dữ liệu nguyên thủy tương ứng.
        */
        byte bytevalue = byteobj;
        short shortvalue = shortobj;
        int intvalue = intobj;
        long longvalue = longobj;
        float floatvalue = floatobj;
        double doublevalue = doubleobj;
        char charvalue = charobj;
        boolean boolvalue = boolobj;

        System.out.println("---Printing primitive values (In gia tri cua cac Primitive Data Types (Kieu du lieu nguyen thuy)---");
        System.out.println("byte value: " + bytevalue);
        System.out.println("short value: " + shortvalue);
        System.out.println("int value: " + intvalue);
        System.out.println("long value: " + longvalue);
        System.out.println("float value: " + floatvalue);
        System.out.println("double value: " + doublevalue);
        System.out.println("char value: " + charvalue);
        System.out.println("boolean value: " + boolvalue);
    }

    public static void main(String[] args) {
        WrapperExample.autoBoxing();
        WrapperExample.unBoxing();
    }
}
